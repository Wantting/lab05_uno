/**
 * ColoredCards 
 */
public abstract class ColoredCards implements Cards{
    protected final Color color;
  
    //cards have different colrs
    enum Color {
        Red, Green, Blue, Yellow;
    }

    public ColoredCards(Color color){
        this.color=color;
    }
   
    
    public String getColor(){
        return this.color.name();
    }
    public abstract boolean canPlay(Cards card);
    public abstract String getName();

    public String toString(){
    	return this.getColor()+" "+this.getName();
    }
}
