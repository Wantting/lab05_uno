/**
 * Cards
 */
public interface Cards {
    boolean canPlay(Cards card);
    String getName();
}