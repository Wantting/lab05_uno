import java.util.ArrayList;
import java.util.Random;

public class Deck {
    private ArrayList<Cards> cardList;

    //constructor
    public Deck(){
        this.cardList = new ArrayList<Cards>();
    }

    public void fullDeck(){
       //added numbered cards
       for (ColoredCards.Color color: ColoredCards.Color.values()) {
        for (int i = 0; i < 2; i++) {
            for (NumCards.Value value : NumCards.Value.values()) {
                //The zero has only four cards
                if(i == 1 && value.equals(NumCards.Value.ZERO)){
                    continue;
                }else{
                    cardList.add(new NumCards(color, value));
                }
            }
        }
    }

       //added special type cards，8 for each specical cards
        for (ColoredCards.Color color: ColoredCards.Color.values()) {
            for (int i = 0; i < 2; i++) {
                cardList.add(new SkipCard(color));
                cardList.add(new ReverseCard(color));
                cardList.add(new PupCard(color));
            }
        }
        
        //added nocolor cards, 4 for each
        for (int i = 0; i < 4; i++) {
            cardList.add(new WildCard());
            cardList.add(new WildPupCard());
        }

    }
    public void addToDeck(Cards card){
        this.cardList.add(card);
    }
 
    public ArrayList<Cards> draw(Cards card){
        this.cardList.remove(card);
        return this.cardList;
    }

    public void shuffle() {
        Random random = new Random();
        Cards tempCard;
        for(int i=this.cardList.size(); i > 1; i--){
            int randIndex = random.nextInt(i);
            tempCard=this.cardList.get(i-1);
            this.cardList.set(i-1, this.cardList.get(randIndex));
            this.cardList.set(randIndex, tempCard);
        }
    }
    //check the size of uno cards
    public int deckSize(){
        return this.cardList.size();
    }
    public Cards getCard(int i){
        return this.cardList.get(i);
      }
    
    public String toString(){
        String listCards = "";
        for (int i = 0; i < this.cardList.size(); i++) {
            listCards+=this.cardList.get(i)+", ";
        }
        return listCards;
    }
}
