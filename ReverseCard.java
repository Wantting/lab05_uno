public class ReverseCard extends ColoredCards{
    private String name;
    public ReverseCard(final Color color){
        super(color);
        this.name = "Reverse";
    }

    public String getName(){
    	return this.name;
    }

    public boolean canPlay(Cards card){
        if(((ColoredCards) card).getColor().equals(this.color.toString())){
           return true; 
        }else if(card.getName().equals(this.name)){
            return true; 
        }else{
            return false;
        }
    }
    
}
