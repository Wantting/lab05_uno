import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class CardsTest {
    @Test
    public void canPlayColorTest() {
        Deck d = new Deck();
        d.fullDeck();
        Cards card = d.getCard(0); //first card is Red ZERO
        NumCards nCards = new NumCards(ColoredCards.Color.Red, NumCards.Value.NINE);
        assertTrue(nCards.canPlay(card));
    }
    @Test
    public void canPlayValueTest() {
        Deck d = new Deck();
        d.fullDeck();
        Cards card = d.getCard(0);//first card is Red ZERO
        NumCards nCards = new NumCards(ColoredCards.Color.Yellow, NumCards.Value.ZERO);
        assertTrue(nCards.canPlay(card));
    }
    @Test
    public void canPlayFalseTest() {
        Deck d = new Deck();
        d.fullDeck();
        Cards card = d.getCard(0);//first card is Red ZERO
        NumCards nCards = new NumCards(ColoredCards.Color.Yellow, NumCards.Value.NINE);
        assertFalse(nCards.canPlay(card));
    }

    @Test
    public void canPlayWildTypeTest() {
        Deck d = new Deck();
        d.fullDeck();
        Cards card = d.getCard(107); //last card is WildPickUpFour
        WildPupCard wCard = new WildPupCard();
        WildCard wildCard = new WildCard();
        assertTrue(wCard.canPlay(card));
        assertFalse(wildCard.canPlay(card));
    }

    @Test
    public void canPlaySpeialTypeTest() {
        Deck d = new Deck();
        d.fullDeck();
        Cards card = d.getCard(76); //this is a red skip card
        SkipCard skipCard = new  SkipCard(ColoredCards.Color.Red);
        assertTrue(skipCard.canPlay(card));
    }
   
    @Test
    // There should be 108 cards in a deck
    public void deckSizeTest(){
        Deck d = new Deck();
        d.fullDeck();
        assertEquals(108, d.deckSize());
    }
    @Test
    public void drawTest() {
        Deck d = new Deck();
        d.fullDeck();
        d.draw(d.getCard(0));
        assertEquals(107, d.deckSize());
    }
    @Test
    public void addCardTest() {
        Deck d = new Deck();
        d.fullDeck();
        Deck c = new Deck();
        c.addToDeck(d.getCard(0));
        assertEquals(d.getCard(0), c.getCard(0));
    }
}
