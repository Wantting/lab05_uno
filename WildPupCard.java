/**
 * WildPupCard
 */
public class WildPupCard implements Cards {
    private String name;
    public WildPupCard(){
        this.name = "WildPickUpFour";
    }

    public String getName(){
    	return this.name;
    }

    public boolean canPlay(Cards card){
        if(card.getName().equals(this.name)){
            return true;
        }else{
            return false;
        }
    }
    public String toString(){
    	return this.getName();
    }
}