public class NumCards extends ColoredCards {
   
    private final Value value;
    enum Value {
        ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE;
    }

    public NumCards(Color color, Value value){
        super(color);
        this.value=value;
    }

    public String getName(){
    	return this.value.name();
    }
    
    public boolean canPlay(Cards card){
        if(((ColoredCards) card).getColor().equals(this.color.toString())){
           return true; 
        }else if(card.getName().equals(this.value.toString())){
            return true; 
        }else{
            return false;
        }
    }

}
