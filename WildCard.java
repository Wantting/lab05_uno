/**
 * WildCard
 */
public class WildCard implements Cards {
    private String name;
    public WildCard(){
        this.name = "Wild";
    }

    public String getName(){
    	return this.name;
    }

    public boolean canPlay(Cards card){
        if(card.getName().equals(this.name)){
            return true;
        }else{
            return false;
        }
    }
    public String toString(){
    	return this.getName();
    }
    
}